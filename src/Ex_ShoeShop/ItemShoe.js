import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { data, handleWatchDetail, handleBuyShoe } = this.props;
    let { image, name } = data;
    return (
      <div className="col-4 p-4">
        <div className="card text-center h-100">
          <img
            className="card-img-top mx-auto"
            style={{ width: 100 }}
            src={image}
            alt={name}
          />
          <div className="card-body p-1">
            <h6 className="card-title">{name}</h6>
          </div>
          <button
            onClick={() => {
              handleWatchDetail(data);
            }}
            className="btn btn-info"
          >
            Detail
          </button>
          <button
            onClick={() => {
              handleBuyShoe(data);
            }}
            className="btn btn-warning"
          >
            Add to Cart
          </button>
        </div>
      </div>
    );
  }
}
