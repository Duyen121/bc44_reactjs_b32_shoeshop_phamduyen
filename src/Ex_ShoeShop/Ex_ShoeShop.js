import React, { Component } from "react";
import { shoeArr } from "./data";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import CartShoe from "./CartShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleViewDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => item.id === shoe.id);
    if (index === -1) {
      let newShoe = { ...shoe, shoeNum: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].shoeNum += 1;
    }
    this.setState({ cart: cloneCart });
  };
  handleDelete = (idShoe) => {
    let cloneCart = this.state.cart.filter((item) => item.id !== idShoe);
    this.setState({ cart: cloneCart });
  };
  handleChangeAmount = (idShoe, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id === idShoe);
    cloneCart[index].shoeNum += option;
    if (cloneCart[index].shoeNum === 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <div className="row">
          <CartShoe
            handleChangeAmount={this.handleChangeAmount}
            handleRemove={this.handleDelete}
            cart={this.state.cart}
          />
          <ListShoe
            handleBuy={this.handleAddToCart}
            handleViewDetail={this.handleViewDetail}
            list={this.state.shoeArr}
          />
        </div>
        <DetailShoe detail={this.state.detailShoe} />
      </div>
    );
  }
}
