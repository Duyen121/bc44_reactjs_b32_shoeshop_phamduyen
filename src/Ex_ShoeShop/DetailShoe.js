import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { name, price, description, quantity } = this.props.detail;
    return (
      <div className="p-4">
        <h2>Detail</h2>
        <table className="table mt-3">
          <thead>
            <tr className="text-center">
              <th className="text-left">Name</th>
              <th>Price</th>
              <th>Description</th>
              <th>Stock</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">{name}</td>
              <td className="text-center">{price}</td>
              <td>{description}</td>
              <td className="text-center">{quantity}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
