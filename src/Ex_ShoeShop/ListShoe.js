import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item) => {
      return (
        <ItemShoe
          handleWatchDetail={this.props.handleViewDetail}
          key={item.id}
          data={item}
          handleBuyShoe={this.props.handleBuy}
        />
      );
    });
  };
  render() {
    return <div className="row col-6">{this.renderListShoe()}</div>;
  }
}
